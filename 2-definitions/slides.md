%title: BAREOS
%author: xavki


# BAREOS : Principes et Définitions


<br>


* système distribuée = différents rôles

* différents paquets 

<br>


* Database : 
		* postgresql (préconisé) ou mysql, stock le catalogue (éventuellement sqlite)
		* utilisée par le directeur 
		* création facilité pour postgres (et par sécurité)
		* contient le catalogue :
				* fichier de conf de bareos
				* informations sur les sauvegardes
				* la catalogue se sauvegarde aussi !!!
		* catalogue = référence
				* les fichiers de conf sont ajoutés en DB au démarrage

-----------------------------------------------------------------------------------

# BAREOS : Principes et Définitions


<br>


* Bareos Director (bareos-dir) : rôle de management des sauvegardes et restaurations
		* communication via un password avec les clients (fd)
<br>


* Bareos Storage Daemon (bareos-sd) : 
		* permet d'écrire les sauvegarde
		* et la lecture pour la restauration
		* accès aux différents stockages

<br>


* Bareos File Daemon (bareos-fd) :
		* client
		* réalise les lectures sur les serveurs distants
		* et les écritures en cas de restauration

<br>


* Bareos Console
		* configuration de bareos
		* fait référence (à la différence des fichiers)
		* utilise la bdd

-----------------------------------------------------------------------------------

# BAREOS : Principes et Définitions


<br>


* Backup : résultat d'un job

<br>


* Job : 
		* tâcche bareos
		* backup/restore avec caractéristiques
				* type (sauvegarde/restauration)
				* level (full/incremental)
				* fileset (source)
				* storage (destination)
		* schedule : programmmation d'un job

<br>


* Volume : 
		* un espace de stockage

<br>


* Pool :
		* ensemble de volumes




https://docs.bareos.org/IntroductionAndTutorial/WhatIsBareos.html#terminology


