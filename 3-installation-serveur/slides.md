%title: BAREOS
%author: xavki


# BAREOS : Installation du serveur


<br>


Doc : https://docs.bareos.org/IntroductionAndTutorial/InstallingBareos.html#section-createdatabase


echo "deb http://download.bareos.org/bareos/release/latest/Debian_9.0/ /" > /etc/apt/sources.list.d/bareos.list
wget -q http://download.bareos.org/bareos/release/latest/Debian_9.0/Release.key -O- | apt-key add --
apt-get update
#apt-get install bareos bareos-database-mysql bareos-webui
apt-get install bareos bareos-database-postgresql bareos-webui
#apt-get install mariadb-server
apt install postgresql
mysql_secure_installation
vim /etc/mysql/mariadb.conf.d/50-client.cnf 
/usr/lib/bareos/scripts/create_bareos_database
/usr/lib/bareos/scripts/make_bareos_tables
/usr/lib/bareos/scripts/grant_bareos_privileges
systemctl start bareos-dir
systemctl start bareos-fd
systemctl start bareos-sd
systemctl restart apache2
bconsole
ip a

