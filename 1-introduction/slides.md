%title: BAREOS
%author: xavki


# BAREOS : introduction


<br>


* Bareos = Backup Archiving REcovery Open Sourced

<br>


* solution de sauvegarde : distribué (ou non)

<br>


* fonctionnement client/server

<br>


* Fork de Bacula (2010) créé par Kern Sibbald

<br>


Doc : https://docs.bareos.org/
Site : https://www.bareos.org/en/

<br>


* principe de simplicité d'utilisation et compréhension

<br>


* possibilité diverses :
		* fichiers simples (images...)
		* systèmes
		* bases de données
		* etc

<br>


* zero knowledge proof (Preuve à divulgation nulle de connaissance) : 
		* prouver que tu détiens quelque chose sans le montrer ;)
		* par déduction
		* https://en.wikipedia.org/wiki/Zero-knowledge_proof

-----------------------------------------------------------------------------------

# BAREOS : introduction


<br>


* dispose d'une interface graphique et d'une console

<br>


* intégration de scripts aux backups possible

<br>


* choix des stockages : uniques ou multiples

<br>


* Licence : GNU AGPLv3 software license

<br>


Paquets disponibles : https://docs.bareos.org/IntroductionAndTutorial/WhatIsBareos.html#bareos-packages
> permet de distribuer le système

<br>


	* bareos 	Backup Archiving REcovery Open Sourced - metapackage
<br>


	* bareos-bconsole 	Bareos administration console (CLI)
<br>


	* bareos-client 	Bareos client Meta-All-In-One package
<br>


	* bareos-database-mysql 	Libs and tools for mysql catalog
<br>


	* bareos-database-postgresql 	Libs and tools for postgresql catalog
<br>


	*	bareos-vmware-plugin 	Bareos VMware plugin
<br>


	* bareos-webui 	Bareos Web User Interface
<br>


	* python-bareos 	Backup Archiving REcovery Open Sourced - Python module


<br>


Vidéo : https://www.youtube.com/watch?v=hfN-4T-jdi8&list=WL
