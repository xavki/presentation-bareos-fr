%title: BAREOS
%author: xavki


# BAREOS : Installation d'un client


<br>


Doc : https://docs.bareos.org/IntroductionAndTutorial/Tutorial.html#adding-a-client


* sur le client

echo "deb http://download.bareos.org/bareos/release/latest/Debian_9.0/ /" > /etc/apt/sources.list.d/bareos.list
wget -q http://download.bareos.org/bareos/release/latest/Debian_9.0/Release.key -O- | apt-key add --
apt update
apt-get install bareos-client


* sur le serveur

```
bconsole
configure add client name=node2 address=192.168.13.11 password=secret
reload
```

* copie du fichier du serveur vers le client

```
cat /etc/bareos/bareos-dir-export/client/node2/bareos-fd.d/director/bareos-dir.conf
vim /etc/bareos/bareos-fd.d/director/bareos-dir.conf
systemctl restart bareos-fd
```


configure add job name=node2-job client=node2 jobdefs=DefaultJob
